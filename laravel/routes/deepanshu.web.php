<?php

Route::get('planes', 'PlanesController@index');

Route::get('planes/{name}', 'PlanesController@show');