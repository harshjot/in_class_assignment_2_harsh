@extends('deepanshu.layout')

@section('content')

      <!-- Post Content Column -->
      <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">Plane Detail</h1>

        <!-- Author -->
        <p class="lead">
          Name: {{$plane->name}}
        </p>

        <p class="lead">
          Description: {{$plane->description}}
        </p>

        <p class="lead">
          <img src="/image/{{$plane->image}}" />
          
        </p>        
      </div>

@endsection