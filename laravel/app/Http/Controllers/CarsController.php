<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cars;

class CarsController extends Controller
{
    public function index()
    {
    	$cars = Cars::carShow();
    	return view('cars.cars', compact('cars'));
    } 

    public function showDetail($id)
    {
    	$car = Cars::where('id', $id)->firstOrFail();
    	return view('cars.show', compact('car'));
    }

}
