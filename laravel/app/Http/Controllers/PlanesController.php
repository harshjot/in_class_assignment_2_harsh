<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Planes;

class PlanesController extends Controller
{
    public function index()
    {
    	$planes = Planes::all();
    	return view('deepanshu/index', compact('planes'));
    }

    public function show($name)
    {
       $plane = Planes::where('name', $name)->firstOrFail();
       return view('deepanshu.show', compact('plane'));
    }
}
