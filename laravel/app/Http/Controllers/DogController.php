<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dog;
class DogController extends Controller
{
    public function index(){
    	$dogs = Dog::tableShow();
    	return view('sonia.index',compact('dogs'));
    }

    public function show($name)
    {
    	$dog = Dog::where('name',$name)->firstOrFail();
    	return view('sonia.show',compact('dog'));
    }
}
