<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(seed_planes_table::class);
        $this->call(seed_dogs_table::class);
        $this->call(seed_cars_table::class);
    }
}
 