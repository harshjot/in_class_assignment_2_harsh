<?php

use Illuminate\Database\Seeder;

class seed_cars_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
        	'name' => 'Tesla',
        	'description' => 'This is the best automatic car',
        	'image' => 'tesla.jpg'
        ]);

        DB::table('cars')->insert([
        	'name' => 'Acura',
        	'description' => 'This is another good car',
        	'image' => 'acura.jpg'
        ]);

        DB::table('cars')->insert([
        	'name' => 'Jeep',
        	'description' => 'This is my favourite car',
        	'image' => 'jeep.jpg'
        ]);

        DB::table('cars')->insert([
        	'name' => 'Bentley',
        	'description' => 'Another Best car',
        	'image' => 'Bentley.jpg'
        ]);

        DB::table('cars')->insert([
        	'name' => 'Duster',
        	'description' => 'Best SUV cars',
        	'image' => 'duster.jpg'
        ]);
    }
}
